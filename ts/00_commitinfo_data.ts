/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartcache',
  version: '1.0.16',
  description: 'cache things in smart ways'
}
